
<?php

	

require_once('disconnect-user.php');


	$curl = curl_init();

	curl_setopt_array($curl, array(
	  CURLOPT_URL => "https://api.bitbay.net/rest/trading/ticker",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "GET",
	  CURLOPT_HTTPHEADER => array(
	    "content-type: application/json"
	  ),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	if ($err) {
	 // echo "cURL Error #:" . $err;
	} else {
	  //echo $response;
	}

	
	$time = time();

	//echo $x;
	//exit;
	$obj = json_decode($response, true);

	$daiPrice = $obj['items']['DAI-PLN']['rate'];
	$btcPrice = $obj['items']['BTC-PLN']['rate'];
	$dashPrice = $obj['items']['DASH-PLN']['rate'];
	$lunaPrice = $obj['items']['LUNA-PLN']['rate'];
	$tronPrice = $obj['items']['TRX-PLN']['rate'];
	$golemPrice = $obj['items']['GNT-PLN']['rate'];
	$sushiPrice = $obj['items']['SUSHI-PLN']['rate'];
	$eosPrice = $obj['items']['EOS-PLN']['rate'];
	$polkaPrice = $obj['items']['DOT-PLN']['rate'];


?>



<!DOCTYPE html >

<html lang="pl">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>E-money mate</title>
	<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Quicksand&display=swap" rel="stylesheet"> 
	<link rel="icon"  href="image/favicon.ico">
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico"><link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
	<link rel="stylesheet" href="style.css">
	</head>
<body>
	<header>
		<main>
			<div class="contentWrapper"></div>
			<div class="logoStyle">
			<h2><a href="welcomePage.php">EM MATE</a></h2>
		</div>
 
			<section class="leftPanel">
				<nav>
					<ul>
					<input type="text" id="search" placeholder="Search...">
					<li><a href="myWallet.php" >MY WALLET</a></li>
					<li><a href="transactions.php">TRANSACTIONS</a></li>
					<li><a href="market.php">MARKET</a></li>					
					<li><a href="settings.php" class="settings">SETTINGS</a></li>
					<li><a href="logout.php">LOG OUT</a></li>
				
				</ul>
				</nav>
			</section>
			<section class="rightPanel">
				<div class="socialInfo"><h4>CHECK NEWS ON MARKET</h4><br>
					</div>
					<div class="moneyStatus">
						DAI: <?php echo $daiPrice ?>
						<br>
						BITCOIN: <?php echo $btcPrice ?>
						<br>
						DASH: <?php echo $dashPrice ?>
						<br>
						TERRA LUNA: <?php echo $lunaPrice ?>
						<br>
						TRON: <?php echo $tronPrice ?>
						<br>
						GOLEM: <?php echo $golemPrice ?>
						<br>
						SUSHITOKEN: <?php echo $sushiPrice ?>
						<br>
						EOS: <?php echo $eosPrice ?>
						<br>
						POLKADOT: <?php echo $polkaPrice ?>
						<br>

						</div>
					
				
			</section>
			</div>
		</main>


</header>
</body>
</html>