<!DOCTYPE html >

<html lang="pl">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>E-money mate</title>
	<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Quicksand&display=swap" rel="stylesheet"> 
	<link rel="icon"  href="image/favicon.ico">
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico"><link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
	<link rel="stylesheet" href="style.css">
	</head>
<body>
	<header>
		<main>
			<div class="contentWrapper"></div>
			<div class="logoStyle">
			<h2><a href="welcomePage.php">EM MATE</a></h2>
		</div>

			<section class="leftPanel">
				<nav>
					<ul>
					<input type="text" id="search" placeholder="Search...">
					<li><a href="myWallet.php" >MY WALLET</a></li>
					<li><a href="transactions.php">TRANSACTIONS</a></li>
					<li><a href="market.php">MARKET</a></li>					
					<li><a href="settings.php" class="settings">SETTINGS</a></li>
          <li><a href="logout.php">LOG OUT</a></li>
				
				</ul>
				</nav>
			</section>
			<section class="rightPanel">
				<div class="socialInfo"><h4>CHECK YOUR TRANSACTIONS</h4><br>
					</div>
					
<div class="transTable">
<table>
  <tr>
    <th>Target</th>
    <th>Date</th>
    <th>Price</th>
  </tr>
  <tr>
    <td>Bitcoin</td>
    <td>22.05.2021</td>
    <td>1.00345$</td>
  </tr>
  <tr>
    <td>DASH</td>
    <td>18.05.2021</td>
    <td>20.954$</td>
  </tr>
   <tr>
    <td>Bitcoin LITE</td>
    <td>18.05.2021</td>
    <td>174.00395$</td>
  </tr>
   <tr>
    <td>Bitcoin</td>
    <td>10.05.2021</td>
    <td>99.00345$</td>
  </tr>
   <tr>
    <td>EOS</td>
    <td>10.06.2021</td>
    <td>19.02$</td>
  </tr>
   <tr>
    <td>Polkadot</td>
    <td>9.05.2021</td>
    <td>1.3577$</td>
  </tr>
  <tr>
    <td>Polkadot</td>
    <td>9.05.2021</td>
    <td>13.45$</td>
  </tr>
  <tr>
    <td>Moonsafe</td>
    <td>9.05.2021</td>
    <td>20.9555$</td>
  </tr>
   <tr>
    <td>Bitcoin</td>
    <td>8.05.2021</td>
    <td>1595.05$</td>
  </tr>
  <tr>
    <td>DASH</td>
    <td>7.05.2021</td>
    <td>1344.5$</td>
  </tr>
  <tr>
    <td>Moonsafe</td>
    <td>6.05.2021</td>
    <td>20.05$</td>
  </tr>
   <tr>
    <td>Bitcoin LITE</td>
    <td>6.05.2021</td>
    <td>155.01$</td>
  </tr>
</table>
</div>
					<br>
				
			</section>
			</div>
		</main>


</header>
</body>
</html>